#include "Bullet.h"
#include "ofAppRunner.h"

Bullet::Bullet()
{
	m_life = false;
	m_vel = 4;
	m_width = 64;
	m_height = 64;

	boom.load("SFX/boom.mp3");
}

Bullet::~Bullet()
{
}

void Bullet::NewBullet(Goblin * g)
{
	m_position = g->GetGoblinPosition() + 16;
	m_life = true;
}

bool Bullet::BulletLife(Player *p, GameStats *g, ofVec2f magicianPos)
{
	if (m_life)
	{
		m_position.x += m_vel;
		m_position.y += -m_vel;
		if (m_position.x > ofGetWidth() || m_position.y < - 60)
		{
			m_life = false;
		}

		if (magicianPos.x > m_position.x + 64 && magicianPos.x + 64 < m_position.x && magicianPos.y > m_position.y + 64 && magicianPos.y + 64 < m_position.y)
		{
			m_life = false;
			boom.play();
			g->changeStats(2);
			return false;
		}
		/*
		ofVec2f villaPos = v->GetPosition();
		if (villaPos.x > m_position.x + 64 && villaPos.x + 64 < m_position.x && villaPos.y > m_position.y + 64 && villaPos.y + 64 < m_position.y)
		{
			m_life = false;
			boom.play();
			return false;
		}*/
	}
	return true;
}

ofVec2f Bullet::GetBulletPos() const
{
	return m_position;
}

#pragma once
#include "GameObject.h"
#include "Goblin.h"
#include "Player.h"
#include "GameStats.h"
#include "ofSoundPlayer.h"

class Bullet : public GameObject
{
private:
	int m_vel;
	bool m_life;
	ofSoundPlayer boom;

public:
	Bullet();
	~Bullet();

	void NewBullet(Goblin *g);
	bool BulletLife(Player *p, GameStats *g, ofVec2f magicianPos);

	ofVec2f GetBulletPos() const;
};
#include "Button.h"
#include "Player.h"
#include "Cursor.h"

Button::Button(int x, int y, int width, int height, int gamestats, string folder)
{
	m_position.x = x;
	m_position.y = y;
	m_width = width;
	m_height = height;
	m_gamestats = gamestats;

	LoadNewAnimation(folder);
}

int Button::PressButton(Player *p)
{
	if (p->GetKeyZ())
	{
		if (m_position.y == p->GetCursorY())
		{
			DrawAnimation();
			p->CursorAnimation();
			return m_gamestats;
		}
		else
		{
			return 10;
		}
	}
	else
	{
		return 10;
	}
}

void Button::DrawButton()
{
	DrawAnimationFrame(0);
}


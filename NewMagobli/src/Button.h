#pragma once
#include "GameObject.h"
#include "Cursor.h"
#include "Player.h"

class Button : public GameObject
{
private:
	int m_gamestats;

public:
	Button(int x, int y, int width, int height, int gamestats, string folder);
	int PressButton(Player *p);

	void DrawButton();
};
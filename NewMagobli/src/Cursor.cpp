#include "Cursor.h"
#include "Player.h"

Cursor::Cursor(int x, int y, int width, int height)
{
	m_position.x = x;
	m_position.y = y;
	m_width = width;
	m_height = height;
	LoadNewAnimation("cursorClick");
	a = 0;
}

void Cursor::DrawCursor()
{
	DrawAnimationFrame(0);
}

void Cursor::ChangeCursor(bool keyUp, bool keyDown)
{
	if (keyUp)
	{
		if (a == 0)
		{
			if (m_position.y > 200)
			{
				m_position.y += -100;
			}
			a++;
		}
	}
	else if (keyDown)
	{
		if (a == 0)
		{
			if (m_position.y < 400)
			{
				m_position.y += 100;
			}
			a++;
		}
	}
	else
		a = 0;
}

int Cursor::GetCursorY() const
{
	return m_position.y;
}

#pragma once
#include "GameObject.h"

class Cursor : public GameObject
{
	friend class Button;
private:
	int a;

public:
	Cursor(int x, int y, int width, int height);

	void DrawCursor();
	void ChangeCursor(bool keyUp, bool keyDown);

	int GetCursorY() const;
};
#include "Energy.h"
#include "ofAppRunner.h"

Energy::Energy()
{
	m_life = false;
	m_vel = 4;
	m_width = 64;
	m_height = 64;

	spell.load("SFX/spell.mp3");
}

void Energy::NewEnergy(Magician *m, bool life)
{
	if (life)
	{
		m_position = m->GetMagicianPos();
		m_count[m->GetCounterEnergy()] = 1;
		m_life = true;
	}
}

void Energy::EnergyLife()
{
	if (m_life)
	{
		m_position.x += -m_vel;
		m_position.y += m_vel;
		if (m_position.x < - 60 || m_position.y > ofGetHeight() - 60)
		{
			m_life = false;
		}
	}
}

void Energy::ByeEnergy()
{
	m_life = false;
}

void Energy::DrawEnergy()
{
	if (m_life)
	{
		GameObject::DrawAnimation();
	}
}

bool Energy::GetEnergyLife() const
{
	return m_life;
}

ofVec2f Energy::GetEnergyPos() const
{
	if (m_life)
	{
		return m_position;
	}
	else
	{
		return ofVec2f(0, 0);
	}
}

#pragma once
#include "GameObject.h"
#include "ofVec2f.h"
#include "Magician.h"

class Energy: public GameObject
{
private:
	bool m_life;
	int m_vel;
	int m_count[3];
	ofSoundPlayer spell;

public:
	Energy();

	void NewEnergy(Magician *m, bool life);
	void EnergyLife();
	void ByeEnergy();

	void DrawEnergy();

	bool GetEnergyLife() const;
	ofVec2f GetEnergyPos() const;

};
#include "GameObject.h"
#include "ofGraphics.h"
#include "ofAppRunner.h"

GameObject::GameObject()
{
	frameIndex = 0;
	m_iFrame = frameIndex;
	m_sequenceFPS = 12;
}

void GameObject::LoadNewImage(string folder, string image)
{
	m_image.load(ofToString(folder) + "/" + ofToString(image));
	std::cout << "Load image: " << image << std::endl;
}

void GameObject::LoadNewAnimation(string folder)
{
	int nImage = m_dir.listDir(ofToString(folder));

	if (nImage) {

		for (int i = 0; i < (m_dir.size()); i++) {

			string filePath = m_dir.getPath(i);
			m_images.push_back(ofImage());
			m_images.back().load(filePath);
		}

		std::cout << "Load folder: " << folder << std::endl;

	}
	else ofLog(OF_LOG_WARNING) << "Could not find folder " << folder;

}

void GameObject::DrawImage()
{
	m_image.draw(m_position.x, m_position.y, m_width, m_height);
}

void GameObject::DrawImage(int x, int y)
{
	m_position.set(x, y);
	m_image.draw(m_position.x, m_position.y, m_width, m_height);
}

void GameObject::DrawAnimation()
{
	if ((int)m_images.size() <= 0) {
		ofSetColor(255);
		ofDrawBitmapString("No Images...", 150, ofGetHeight() / 2);
	}

	m_iFrame = (int)(ofGetElapsedTimef() * m_sequenceFPS) % m_images.size();
	m_images[m_iFrame].draw(m_position.x, m_position.y, m_width, m_height);
}

void GameObject::DrawAnimation(int x, int y)
{
	if ((int)m_images.size() <= 0) {
		ofSetColor(255);
		ofDrawBitmapString("No Images...", 150, ofGetHeight() / 2);
	}

	m_iFrame = (int)(ofGetElapsedTimef() * m_sequenceFPS) % m_images.size();
	m_images[m_iFrame].draw(x, y, m_width, m_height);
}

void GameObject::DrawAnimation(int x, int y, int w, int h)
{
	if ((int)m_images.size() <= 0) {
		ofSetColor(255);
		ofDrawBitmapString("No Images...", 150, ofGetHeight() / 2);
	}

	m_iFrame = (int)(ofGetElapsedTimef() * m_sequenceFPS) % m_images.size();
	m_images[m_iFrame].draw(x, y, w, h);
}

void GameObject::DrawAnimationFrame(int frame)
{
	m_images[frame].draw(m_position.x, m_position.y, m_width, m_height);
}

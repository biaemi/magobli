#include "Goblin.h"
#include "ofAppRunner.h"

Goblin::Goblin()
{
	m_life = false;
	m_width = 80;
	m_height = 80;
	a = 0;
	m_vel = 0;
}

Goblin::~Goblin()
{
}

void Goblin::NewGoblin(int x, int y)
{
	if (a == 0)
	{
		m_life = true;
		m_position.set(x, y);
		a++;
	}
}

void Goblin::NewGoblin(int time, Stone * s[10])
{
	if (m_life == false && time % 3 == 0 && time != 0)
	{
		int a = 1 + rand() % 8;
		m_position = s[a]->GetStonePos();
		m_vel = s[a]->GetStoneVel();
		m_life = true;
	}
}

void Goblin::GoblinLife(Energy *e[3], Player *p, Magician *m)
{
	if (m_life)
	{
		if (m_position.x < 0 - 256)
		{
			m_position.x = ofGetWidth();
			m_position.x += m_vel;
		}
		else if (m_position.x > ofGetWidth())
		{
			m_position.x = 0 - 256;
			m_position.x += m_vel;
		}

		m_position.x += m_vel;

		for (int a = 0; a < 3; a++)
		{
			if (e[a]->GetEnergyLife())
			{
				ofVec2f energy = e[a]->GetEnergyPos();
				if (energy.x > m_position.x - 8 && energy.x < m_position.x + 40 && energy.y > m_position.y - 8 && energy.y < m_position.y + 40)
				{
					m_life = false;
					e[a]->ByeEnergy();
					p->AddPoints();
				}
			}
		}

		ofVec2f magician = m->GetMagicianPos();
		if (magician.x > m_position.x - 60 && magician.x < m_position.x + 80 && magician.y > m_position.y - 60 && magician.y < m_position.y + 80)
		{
			m->ByeMagician();
		}

	}
}

void Goblin::DrawGoblin()
{
	if (m_life)
	{
		GameObject::DrawAnimation();
	}
}

void Goblin::DrawGoblin(int x)
{
	if (m_life)
	{
		GameObject::DrawAnimationFrame(x);
	}
}

ofVec2f Goblin::GetGoblinPosition() const
{
	return m_position;
}

bool Goblin::GetGoblinLife() const
{
	return m_life;
}

#pragma once
#include "GameObject.h"
#include "Stone.h"
#include "Energy.h"

class Goblin: public GameObject
{
private:
	bool m_life;
	int m_vel;
	int a;

public:
	Goblin();
	~Goblin();

	void NewGoblin(int x, int y);
	void NewGoblin(int time, Stone *s[10]);
	void GoblinLife(Energy *e[3], Player *p, Magician *m);
	void DrawGoblin();
	void DrawGoblin(int x);

	ofVec2f GetGoblinPosition() const;
	bool GetGoblinLife() const;

};
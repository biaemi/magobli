#include "Magician.h"
#include "ofAppRunner.h"
#include "ofVec2f.h"
#include "ofGraphics.h"

Magician::Magician()
{
	m_counter = 0;
	m_width = 64;
	m_height = 64;
	m_position.set(960, 0);
	m_life = true;
	m_vel = 0;

	m_press[0] = 0;
	m_press[1] = 0;
	m_press[2] = 0;
	m_press[3] = 0;
	m_press[4] = 0;
	m_press[5] = 0;

	walk.load("SFX/walk.mp3");
	lavaMorte.load("SFX/lavaMorte.mp3");


	int nImage = m_dir.listDir(ofToString("magicianWalk"));

	if (nImage) {

		for (int i = 0; i < (m_dir.size()); i++) {

			string filePath = m_dir.getPath(i);
			m_walkAni.push_back(ofImage());
			m_walkAni.back().load(filePath);
		}

		std::cout << "Load folder: magicianWalk" << std::endl;
	}
	else ofLog(OF_LOG_WARNING) << "Could not find folder " << "magicianWalk";


	nImage = m_dir.listDir(ofToString("magicianShoot"));

	if (nImage) {

		for (int i = 0; i < (m_dir.size()); i++) {

			string filePath = m_dir.getPath(i);
			m_shootAni.push_back(ofImage());
			m_shootAni.back().load(filePath);
		}

		std::cout << "Load folder: magicianShoot" << std::endl;
	}
	else ofLog(OF_LOG_WARNING) << "Could not find folder " << "magicianShoot";


	nImage = m_dir.listDir(ofToString("magicianEnergy"));

	if (nImage) {

		for (int i = 0; i < (m_dir.size()); i++) {

			string filePath = m_dir.getPath(i);
			m_energyAni.push_back(ofImage());
			m_energyAni.back().load(filePath);
		}

		std::cout << "Load folder: magicianEnergy" << std::endl;
	}
	else ofLog(OF_LOG_WARNING) << "Could not find folder " << "magicianEnergy";
}

void Magician::MagicianMove(Player * p)
{
	if (m_press[0] == 0)
	{
		if (p->GetKeyUp() == true && m_position.y > 0)
		{
			m_position.y += -64;
			walk.play();
			m_press[0]++;
		}
	}
	else
	{
		if (p->GetKeyUp() == false)
		{
			m_press[0] = 0;
		}
	}
	if (m_press[1] == 0)
	{
		if (p->GetKeyDown() == true && m_position.y < ofGetHeight())
		{
			m_position.y += 64;
			walk.play();
			m_press[1] ++;
		}
	}
	else
	{
		if (p->GetKeyDown() == false)
		{
			m_press[1] = 0;
		}
	}
	if (m_press[2] == 0)
	{
		if (p->GetKeyRight() == true && m_position.x < ofGetWidth())
		{
			m_position.x += 64;
			walk.play();
			m_press[2]++;
		}
	}
	else
	{
		if (p->GetKeyRight() == false)
		{
			m_press[2] = 0;
		}
	}
	if (m_press[3] == 0)
	{
		if (p->GetKeyLeft() == true && m_position.x > 0)
		{
			m_position.x += -64;
			walk.play();
			m_press[3]++;
		}
	}
	else
	{
		if (p->GetKeyLeft() == false)
		{
			m_press[3] = 0;
		}
	}
}

bool Magician::CreateEnergy(Player * p)
{
	if (m_press[4] == 0)
	{
		if (p->GetKeyX() == true && m_counter < 3)
		{
			m_counter++;
			m_energy = true;
			return m_energy;
		}
		else
		{
			m_energy = false;
			return m_energy;
		}
		m_press[4]++;
	}
	else
	{
		if (p->GetKeyX() == false)
		{
			m_press[4] = 0;
		}
	}
}

void Magician::MagicianLife(GameStats * g)
{
	if (m_position.y > 6 * 64)
	{
		m_life == false;
	}
}

void Magician::MagicianLife(Stone * s[10], Player *p, GameStats *g)
{
	if (g->GetEstado() == 1)
	{
		if (p->GetLevel() < 3)
		{
			if (m_position.y > 6 * 64)
			{
				m_life = false;
			}
		}
		else
		{
			if (m_position.y == 0 || m_position.y == 64 * 11)
			{
				m_life = true;
				m_vel = 0;
			}
			else
			{
				for (int a = 0; a < 10; a++)
				{
					ofVec2f stone = s[a]->GetStonePos();
					if (m_position.y == stone.y)
					{
						if (m_position.x > stone.x && m_position.x < stone.x + 256)
						{
							m_num = s[a]->GetNumb();
							m_vel = s[a]->GetStoneVel();
							m_life = true;
							p->AddPoints();
						}
						else
						{
							m_life = false;
							lavaMorte.play();
							g->changeStats(3);
						}
					}
				}
			}

		}
	}
	else if (g->GetEstado() == 2)
	{
		if (m_position.y == 0 || m_position.y == 64 * 11)
		{
			m_life = true;
			m_vel = 0;
		}
		else
		{
			for (int a = 0; a < 10; a++)
			{
				ofVec2f stone = s[a]->GetStonePos();
				if (m_position.y == stone.y)
				{
					if (m_position.x > stone.x && m_position.x < stone.x + 256)
					{
						m_num = s[a]->GetNumb();
						m_vel = s[a]->GetStoneVel();
						m_life = true;
						p->AddPoints();
					}
					else
					{
						m_life = false;
						lavaMorte.play();
						g->changeStats(3);
					}
				}
			}
		}
	}
}

void Magician::ByeMagician()
{
	m_life = false;
}

void Magician::MagicianUpdate()
{
	m_position.x += m_vel;
}

void Magician::DrawMagician(Player *p)
{
	if (p->GetKeyUp() || p->GetKeyDown() || p->GetKeyLeft() || p->GetKeyRight())
	{
		if ((int)m_walkAni.size() <= 0) {
			ofSetColor(255);
			ofDrawBitmapString("No Images...", 150, ofGetHeight() / 2);
		}

		m_iFrame = (int)(ofGetElapsedTimef() * m_sequenceFPS) % m_walkAni.size();
		m_walkAni[m_iFrame].draw(m_position.x, m_position.y, m_width, m_height);
	}
	else if (p->GetKeyX())
	{
		if ((int)m_shootAni.size() <= 0) {
			ofSetColor(255);
			ofDrawBitmapString("No Images...", 150, ofGetHeight() / 2);
		}

		m_iFrame = (int)(ofGetElapsedTimef() * m_sequenceFPS) % m_shootAni.size();
		m_shootAni[m_iFrame].draw(m_position.x, m_position.y, m_width, m_height);

		if ((int)m_energyAni.size() <= 0) {
			ofSetColor(255);
			ofDrawBitmapString("No Images...", 150, ofGetHeight() / 2);
		}

		m_iFrame = (int)(ofGetElapsedTimef() * m_sequenceFPS) % m_energyAni.size();
		m_energyAni[m_iFrame].draw(m_position.x, m_position.y, m_width, m_height);
	}
	else
	{
		m_walkAni[0].draw(m_position);
	}
}

ofVec2f Magician::GetMagicianPos() const
{
	return m_position;
}

int Magician::GetCounterEnergy() const
{
	return m_counter;
}

#pragma once
#include "GameObject.h"
#include "ofVec2f.h"
#include "ofSoundPlayer.h"
#include "Player.h"
#include "Stone.h"
#include "GameStats.h"

class Magician : public GameObject
{
private:
	int m_counter;
	bool m_energy;
	bool m_life;
	int m_vel, m_press[6];
	int m_num;
	ofSoundPlayer walk;
	ofSoundPlayer lavaMorte;

	vector <ofImage> m_walkAni;
	vector <ofImage> m_shootAni;
	vector <ofImage> m_energyAni;

public:

	Magician();

	void MagicianMove(Player *p);
	bool CreateEnergy(Player *p);
	void MagicianLife(GameStats *g);
	void MagicianLife(Stone *s[10], Player *p, GameStats *g);
	void ByeMagician();
	void MagicianUpdate();

	void DrawMagician(Player *p);

	ofVec2f GetMagicianPos() const;
	int GetCounterEnergy() const;

};
#include "Player.h"

Player::Player(int x, int y, int width, int height)
{
	m_position.x = x;
	m_position.y = y;
	m_width = width;
	m_height = height;
	m_lvl = 0;

	m_keyUp = false;
	m_keyDown = false;
	m_keyLeft = false;
	m_keyRight = false;
	m_keyX = false;
	m_keyZ = false;


	plin.load("SFX/plin.mp3");
	m_cursor = new Cursor(790, 200, 36, 54);
}

void Player::Press(int a)
{
	switch (a)
	{
	case 0:
		m_keyUp = true;
		break;
	case 1:
		m_keyDown = true;
		break;
	case 2:
		m_keyRight = true;
		break;
	case 3:
		m_keyLeft = true;
		break;
	case 4:
		m_keyZ = true;
		break;
	case 5:
		m_keyX = true;
		break;
	default:
		break;
	}
}
void Player::Release(int a)
{
	switch (a)
	{
	case 0:
		m_keyUp = false;
		break;
	case 1:
		m_keyDown = false;
		break;
	case 2:
		m_keyRight = false;
		break;
	case 3:
		m_keyLeft = false;
		break;
	case 4:
		m_keyZ = false;
		break;
	case 5:
		m_keyX = false;
		break;
	default:
		break;
	}
}

void Player::UpdateMenu()
{
	m_cursor->ChangeCursor(m_keyUp, m_keyDown);
}

void Player::AddPoints()
{
	m_points += 10;
	if (m_points % 100 == 0)
	{
		plin.play();
	}
}

void Player::NextLevel()
{
	m_lvl++;
	std::cout << m_lvl << std::endl;
}

void Player::DrawAnimationFrame(int x)
{
	m_cursor->DrawAnimationFrame(x);
}

bool Player::GetKeyZ() const
{
	return m_keyZ;
}

bool Player::GetKeyUp() const
{
	return m_keyUp;
}

bool Player::GetKeyDown() const
{
	return m_keyDown;
}

bool Player::GetKeyRight() const
{
	return m_keyRight;
}

bool Player::GetKeyLeft() const
{
	return m_keyLeft;
}

int Player::GetLevel() const
{
	return m_lvl;
}

bool Player::GetKeyX() const
{
	return m_keyX;
}

int Player::GetCursorY() const
{
	return m_cursor->GetCursorY();
}

void Player::CursorAnimation()
{
	m_cursor->DrawAnimation();
}

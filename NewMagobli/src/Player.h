#pragma once
#include "GameObject.h"
#include "ofSoundPlayer.h"
#include "Cursor.h"

class Player : public GameObject
{
private:
	bool m_keyRight;
	bool m_keyLeft;
	bool m_keyUp;
	bool m_keyDown;
	bool m_keyZ;
	bool m_keyX;
	int m_points;
	int m_lvl;

	Cursor *m_cursor;
	ofSoundPlayer plin;

public:
	Player(int x, int y, int width, int height);

	void Press(int a);
	void Release(int a);
	
	void UpdateMenu();
	void AddPoints();
	void NextLevel();

	void DrawAnimationFrame(int x);

	bool GetKeyZ() const;
	bool GetKeyUp() const;
	bool GetKeyDown() const;
	bool GetKeyRight() const;
	bool GetKeyLeft() const;
	int GetLevel() const;
	bool GetKeyX() const;

	int GetCursorY() const;
	void CursorAnimation();
};

#include "Portal.h"

Portal::Portal()
{
	m_draw = false;

	m_width = 64;
	m_height = 64;

	m_counterGoblin = 0;
	m_counterVillagers = 0;
}

void Portal::ChangePortalStats(bool draw)
{
	m_draw = draw;
}

void Portal::DefinePosition(ofVec2f newPos)
{
	m_position = newPos;
}

void Portal::PortalMagian(ofVec2f magPos, Player * p)
{
	if (m_counterVillagers >= m_villagers && m_counterGoblin >= m_goblin)
	{
		if (m_draw == true && m_position == magPos && p->GetKeyZ())
		{
			p->NextLevel();
		}
	}
}

void Portal::PortalMagian(ofVec2f magPos, Player *p, string ok)
{
	if (m_draw == true && magPos.x > m_position.x - 8 && magPos.x < m_position.x + 40 && magPos.y > m_position.y - 8 && magPos.y < m_position.y + 40)
	{
		p->NextLevel();
	}
}

void Portal::GameGoal(Player * p, Goblin * g[4], ofVec2f magPos)
{
	if (m_position == magPos && p->GetKeyZ())
	{
		m_counterVillagers++;
	}
	for (int a = 0; a < m_goblin; a++)
	{
		if (g[a]->GetGoblinLife())
		{
			m_counterGoblin++;
		}
	}
	if (m_counterGoblin >= m_goblin)
	{
		m_draw = true;
	}
}

void Portal::DefineGameGoal(int villagers, int goblins)
{
	m_villagers = villagers;
	m_goblin = goblins;
}

void Portal::DrawPortal(int x, int y)
{
	if (m_draw)
	{
		m_position.set(x, y);
		GameObject::DrawAnimation(x, y);
	}
}

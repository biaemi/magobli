#pragma once
#include "GameObject.h"
#include "Player.h"
#include "Goblin.h"

class Portal : public GameObject
{
private:
	bool m_draw;
	int m_counterVillagers, m_villagers, m_counterGoblin, m_goblin;

public:
	Portal();
	void ChangePortalStats(bool draw);

	void DefinePosition(ofVec2f newPos);
	void PortalMagian(ofVec2f magPos, Player *p);
	void PortalMagian(ofVec2f magPos, Player *p, string ok);
	void GameGoal(Player *p, Goblin *g[4], ofVec2f magPos);
	void DefineGameGoal(int villagers, int goblins);

	void DrawPortal(int x, int y);
};
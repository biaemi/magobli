#include "Stone.h"
#include "ofAppRunner.h"

Stone::Stone(int k)
{
	m_width = 256;
	m_height = 64;
	m_vel = 2 + rand() % 5;
	m_position.y = 64 + (64 * k);
	int i = k % 2;
	m_num = k;

	if (i == 0)
	{
		m_position.x = 0;
	}
	else
	{
		m_position.x = ofGetWidth();
		m_vel = m_vel * (-1);
	}
}

void Stone::StoneUpdate()
{
	if (m_position.x < 0 - 256)
	{
		m_position.x = ofGetWidth();
		m_position.x += m_vel;
	}
	else if (m_position.x > ofGetWidth())
	{
		m_position.x = 0 - 256;
		m_position.x += m_vel;
	}

	m_position.x += m_vel;
}

ofVec2f Stone::GetStonePos() const
{
	return m_position;
}

int Stone::GetStoneVel() const
{
	return m_vel;
}

int Stone::GetNumb() const
{
	return m_num;
}

#pragma once
#include "GameObject.h"

class Stone : public GameObject
{
private:
	int m_vel;
	int m_num;
public:
	Stone(int k);

	void StoneUpdate();

	ofVec2f GetStonePos() const;
	int GetStoneVel() const;
	int GetNumb() const;
};
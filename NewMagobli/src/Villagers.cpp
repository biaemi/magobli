#include "Villagers.h"

Villagers::Villagers()
{
	m_position.set(8 * 64, 0);
	m_width = 64;
	m_height = 64;

	m_follow = false;

	m_press[0] = 0;
	m_press[1] = 0;
	m_press[2] = 0;
	m_press[3] = 0;
}

void Villagers::FollowMagician(Player * p, ofVec2f magPos)
{
	if (p->GetKeyZ() == true && magPos == m_position)
	{
		m_follow = !m_follow;
	}
}

void Villagers::UpdatePosition(Player * p, ofVec2f magPos)
{
	if (m_follow)
	{
		if (m_press[0] == 0)
		{
			if (p->GetKeyUp())
			{
				m_position.y += -64;
				m_press[0]++;
			}
		}
		else
		{
			if (p->GetKeyUp() == false)
			{
				m_press[0] = 0;
			}
		}
		if (m_press[1] == 0)
		{
			if (p->GetKeyDown())
			{
				m_position.y += 64;
				m_press[1]++;
			}
		}
		else
		{
			if (p->GetKeyDown() == false)
			{
				m_press[1] = 0;
			}
		}
		if (m_press[2] == 0)
		{
			if (p->GetKeyRight())
			{
				if (m_position != magPos)
				{
					m_position.x += 64;
					m_press[2]++;
				}
			}
		}
		else
		{
			if (p->GetKeyRight() == false)
			{
				m_press[2] = 0;
			}
		}
		if (m_press[3] == 0)
		{
			if (p->GetKeyLeft())
			{
				if (m_position != magPos)
				{
					m_position.x += -64;
					m_press[3]++;
				}
			}
		}
		else
		{
			if (p->GetKeyLeft() == false)
			{
				m_press[3] = 0;
			}
		}
	}
}

ofVec2f Villagers::GetPosition() const
{
	return m_position;
}

#pragma once
#include "GameObject.h"
#include "Player.h"

class Villagers : public GameObject
{
private:
	bool m_follow, m_life;
	int m_press[4];

public:
	Villagers();

	void FollowMagician(Player *p, ofVec2f magPos);
	void UpdatePosition(Player *p, ofVec2f magPos);

	ofVec2f GetPosition() const;
};
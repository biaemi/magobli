#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
	ofBackground(0);
	ofSetWindowTitle("Magobli");

	k = 0;
	magobli.load("images/Magobli.png");
	lava.load("images/Lava.png");
	fundo.load("images/Fundo.png");
	gameover.load("images/gameover.png");
	menu.load("SFX/AgoraVai.mp3");
	magobliTheme.load("SFX/Magobli-Theme.mp3");

	p = new Player(960, 0, 64, 64);

	gameO = new GameObject();
	gameS = new GameStats();
	gameT = new GameTime();
	start = new Button(600, 200, 220, 52, 2, "buttonStart");
	tutorial = new Button(600, 300, 220, 52, 1, "buttonTutorial");
	exit = new Button(600, 400, 220, 52, 0, "buttonExit");

	tutoKeyUp = new GameObject();
	tutoKeyUp->LoadNewAnimation("keyUp");

	tutoKeyDown = new GameObject();
	tutoKeyDown->LoadNewAnimation("keyDown");

	tutoKeyRight = new GameObject();
	tutoKeyRight->LoadNewAnimation("keyRight");

	tutoKeyLeft = new GameObject();
	tutoKeyLeft->LoadNewAnimation("keyLeft");

	tutoKeyZ = new GameObject();
	tutoKeyZ->LoadNewAnimation("keyZ");

	tutoKeyX = new GameObject();
	tutoKeyX->LoadNewAnimation("keyX");

	terra.load("images/Terra.png");
	terra1.load("images/Terra1.png");

	for (int a = 0; a < 10; a++)
	{
		stone[a] = new Stone(a);
		stone[a]->LoadNewImage("images", "P1.png");
	}
	for (int b = 0; b < 3; b++)
	{
		ene[b] = new Energy();
		ene[b]->LoadNewAnimation("energyLoop");
	}

	magician = new Magician();

	portal = new Portal();
	portal->LoadNewAnimation("portal");

	for (int c = 0; c < 4; c++)
	{
		villa[c] = new Villagers();
	}
	villa[0]->LoadNewImage("villagers", "Cat.png");
	villa[1]->LoadNewImage("villagers", "Danilo.png");
	villa[2]->LoadNewImage("villagers", "Doge.png");
	villa[3]->LoadNewImage("villagers", "Baby.png");
	for (int c = 0; c < 4; c++)
	{
		bul[c] = new Bullet();
	}
	for (int c = 0; c < 4; c++)
	{
		gabs[c] = new Goblin();
	}
	for (int c = 0; c < 4; c++)
	{
		bul[c]->LoadNewAnimation("ghost");
		gabs[c]->LoadNewAnimation("goblin");
	}

	gameS->changeStats(0);

	std::cout << "Setup ok!" << std::endl;
}

//--------------------------------------------------------------
void ofApp::update() {
	int a, b, t;
	switch (gameS->GetEstado())
	{
	case 0:
#pragma region UPDATE_MENU
		std::cout << "Update Menu start" << std::endl;
		if (x == 0)
		{
			menu.setLoop(true);
			menu.play();
			x++;
		}

		p->UpdateMenu();
		a = start->PressButton(p);
		t = tutorial->PressButton(p);
		b = exit->PressButton(p);

		if (a != 10)
		{
			gameS->changeStats(a);
			menu.stop();
			x = 0;
		}
		else if (b != 10)
		{
			gameS->changeStats(b);
			menu.stop();
			x = 0;
		}
		else if (t != 10)
		{
			gameS->changeStats(t);
			menu.stop();
			x = 0;
		}
		else
		{
			gameS->changeStats(0);
		}
		std::cout << "Update Menu end" << std::endl;
#pragma endregion 
		break;
	case 1:
#pragma region UPDATE_TUTORIAL
		std::cout << "Update tutorial start" << std::endl;
		if (x == 0)
		{
			magobliTheme.setLoop(true);
			magobliTheme.play();
			x++;
		}
		//Magician vai para o portal __OK__
		if (p->GetLevel() == 0)
		{
			numbV = 0;
			numbG = 0;
			portal->DefineGameGoal(numbV, numbG);
			portal->GameGoal(p, gabs, magician->GetMagicianPos());
			portal->PortalMagian(magician->GetMagicianPos(), p);

#pragma region UPDATE_MAGICIAN
			magician->MagicianMove(p);
			magician->MagicianLife(stone, p, gameS);
#pragma endregion

		}
		// Magician leva pessoas ao portal __OK__
		else if (p->GetLevel() == 1)
		{
			numbV = 1;
			numbG = 0;
			portal->DefineGameGoal(numbV, numbG);
			portal->GameGoal(p, gabs, magician->GetMagicianPos());
			portal->PortalMagian(magician->GetMagicianPos(), p);

#pragma region UPDATE_MAGICIAN
			magician->MagicianMove(p);
			magician->MagicianLife(stone, p, gameS);
#pragma endregion

#pragma region UPDATE_VILLAGERS
			for (int i = 0; i < numbV; i++)
			{
				villa[i]->FollowMagician(p, magician->GetMagicianPos());
				villa[i]->UpdatePosition(p, magician->GetMagicianPos());
			}
#pragma endregion

		}
		// Magician atira no goblin para o portal aparecer 
		else if (p->GetLevel() == 2)
		{
			numbV = 0;
			numbG = 1;
#pragma region UPDATE_PORTAL
			portal->DefineGameGoal(numbV, numbG);
			portal->GameGoal(p, gabs, magician->GetMagicianPos());
			portal->PortalMagian(magician->GetMagicianPos(), p);
#pragma endregion

#pragma region UPDATE_MAGICIAN_&_ENERGY
			magician->MagicianMove(p);
			magician->MagicianLife(stone, p, gameS);

			for (int c = 0; c < 3; c++)
			{
				if (ene[c]->GetEnergyLife() == false && magician->CreateEnergy(p))
				{
					ene[magician->GetCounterEnergy() - 1]->NewEnergy(magician, magician->CreateEnergy(p));
				}
				else
				{
					ene[c]->EnergyLife();
				}
			}
#pragma endregion

#pragma region UPDATE_GOBLINS
			for (int i = 0; i < numbG; i++)
			{
				gabs[i]->NewGoblin(6 * 64-16, 4 * 64-16);
				gabs[i]->GoblinLife(ene, p, magician);
			}
#pragma endregion 

		}
		// Magician atravessa o rio de lava
		else if (p->GetLevel() == 3)
		{
			numbV = 0;
			numbG = 0;
			portal->DefineGameGoal(numbV, numbG);
			portal->GameGoal(p, gabs, magician->GetMagicianPos());
			portal->PortalMagian(magician->GetMagicianPos(), p);

#pragma region UPDATE_MAGICIAN_&_ENERGY
			magician->MagicianMove(p);
			magician->MagicianLife(stone, p, gameS);
			magician->MagicianUpdate();

			for (int c = 0; c < 3; c++)
			{
				if (ene[c]->GetEnergyLife() == false && magician->CreateEnergy(p))
				{
					ene[magician->GetCounterEnergy() - 1]->NewEnergy(magician, magician->CreateEnergy(p));
				}
				else
				{
					ene[c]->EnergyLife();
				}
				if (c > 0 && c < 3)
				{
					if (ene[c - 1]->GetEnergyLife() == false && ene[c]->GetEnergyLife() == true)
					{
						ene[c - 1] = ene[c];
					}
					if (ene[c - 1]->GetEnergyLife() == false && ene[c + 1]->GetEnergyLife() == true)
					{
						ene[c - 1] = ene[c + 1];
					}
				}
			}
#pragma endregion

#pragma region UPDATE_BACKGROUND
			for (int i = 0; i < 10; i++)
			{
				stone[i]->StoneUpdate();
			}
#pragma endregion

		}
		else
		{
			gameS->changeStats(0);
			magobliTheme.setLoop(false);
			magobliTheme.stop();
			x = 0;
		}
		break;

		std::cout << "Update tutorial end" << std::endl;
#pragma endregion
	case 2:
#pragma region UPDATE_GAME
		std::cout << "Update game start" << std::endl;
		if (x == 0)
		{
			magobliTheme.setLoop(true);
			magobliTheme.play();
			x++;
		}
		// Magician atravessa o rio de lava
		if (p->GetLevel() == 0)
		{
			if (k == 0)
			{
				k++;
				gameT->StartTime();
			}

			numbV = 0;
			numbG = 0;

			portal->DefineGameGoal(numbV, numbG);
			portal->GameGoal(p, gabs, magician->GetMagicianPos());
			portal->PortalMagian(magician->GetMagicianPos(), p);

		}
		// Magician atravessa o rio de lava com uma pessoa
		else if (p->GetLevel() == 1)
		{
			if (k == 1)
			{
				k--;
				gameT->StartTime();
			}

			numbV = 1;
			numbG = 0;

			portal->DefineGameGoal(numbV, numbG);
			portal->GameGoal(p, gabs, magician->GetMagicianPos());
			portal->PortalMagian(magician->GetMagicianPos(), p);

#pragma region UPDATE_VILLAGERS
			for (int i = 0; i < numbV; i++)
			{
				villa[i]->FollowMagician(p, magician->GetMagicianPos());
				villa[i]->UpdatePosition(p, magician->GetMagicianPos());
			}
#pragma endregion
		}
		// Magician mata um Goblin para o portal aparecer
		else if (p->GetLevel() == 2)
		{
			if (k == 0)
			{
				k++;
				gameT->StartTime();
			}

			numbV = 0;
			numbG = 1;

			portal->DefineGameGoal(numbV, numbG);
			portal->GameGoal(p, gabs, magician->GetMagicianPos());
			portal->PortalMagian(magician->GetMagicianPos(), p);
#pragma region UPDATE_GOBLIN
			for (int c = 0; c < numbG; c++)
			{
				gabs[c]->NewGoblin(gameT->GetTimeGame(), stone);
				gabs[c]->GoblinLife(ene, p, magician);
			}
#pragma endregion 
		}
		// Magician mata dois Goblins para o portal aparecer
		else if (p->GetLevel() == 3)
		{
			if (k == 1)
			{
				k--;
				gameT->StartTime();
			}

			numbV = 0;
			numbG = 2;

			portal->DefineGameGoal(numbV, numbG);
			portal->GameGoal(p, gabs, magician->GetMagicianPos());
			portal->PortalMagian(magician->GetMagicianPos(), p);
#pragma region UPDATE_GOBLIN
			for (int c = 0; c < numbG; c++)
			{
				gabs[c]->NewGoblin(gameT->GetTimeGame(), stone);
				gabs[c]->GoblinLife(ene, p, magician);
			}
#pragma endregion 
		}
		// Magician mata 3 Goblins e salva 1 pessoa
		else if (p->GetLevel() == 4)
		{
			if (k == 0)
			{
				k++;
				gameT->StartTime();
			}

			numbV = 1;
			numbG = 3;

			portal->DefineGameGoal(numbV, numbG);
			portal->GameGoal(p, gabs, magician->GetMagicianPos());
			portal->PortalMagian(magician->GetMagicianPos(), p);


#pragma region UPDATE_VILLAGERS
			for (int i = 0; i < numbV; i++)
			{
				villa[i]->FollowMagician(p, magician->GetMagicianPos());
				villa[i]->UpdatePosition(p, magician->GetMagicianPos());
			}
#pragma endregion

#pragma region UPDATE_GOBLIN
			for (int c = 0; c < numbG; c++)
			{
				gabs[c]->NewGoblin(gameT->GetTimeGame(), stone);
				gabs[c]->GoblinLife(ene, p, magician);
			}
#pragma endregion
		}
		//Magician mata 1 goblin e salva 2 pessoas
		else if (p->GetLevel() == 5)
		{
			if (k == 1)
			{
				k--;
				gameT->StartTime();
			}

			numbV = 2;
			numbG = 1;

			portal->DefineGameGoal(numbV, numbG);
			portal->GameGoal(p, gabs, magician->GetMagicianPos());
			portal->PortalMagian(magician->GetMagicianPos(), p);


#pragma region UPDATE_VILLAGERS
			for (int i = 0; i < numbV; i++)
			{
				villa[i]->FollowMagician(p, magician->GetMagicianPos());
				villa[i]->UpdatePosition(p, magician->GetMagicianPos());
			}
#pragma endregion

#pragma region UPDATE_GOBLIN
			for (int c = 0; c < numbG; c++)
			{
				gabs[c]->NewGoblin(gameT->GetTimeGame(), stone);
				gabs[c]->GoblinLife(ene, p, magician);
			}
#pragma endregion 
		}
		// Magician mata 2 Goblins e salva 2 pessoas
		else if (p->GetLevel() == 6)
		{
			if (k == 0)
			{
				k++;
				gameT->StartTime();
			}

			numbV = 2;
			numbG = 2;

			portal->DefineGameGoal(numbV, numbG);
			portal->GameGoal(p, gabs, magician->GetMagicianPos());
			portal->PortalMagian(magician->GetMagicianPos(), p);


#pragma region UPDATE_VILLAGERS
			for (int i = 0; i < numbV; i++)
			{
				villa[i]->FollowMagician(p, magician->GetMagicianPos());
				villa[i]->UpdatePosition(p, magician->GetMagicianPos());
			}
#pragma endregion

#pragma region UPDATE_GOBLIN
			for (int c = 0; c < numbG; c++)
			{
				gabs[c]->NewGoblin(gameT->GetTimeGame(), stone);
				gabs[c]->GoblinLife(ene, p, magician);
			}
#pragma endregion 
		}
		else
		{
			gameS->changeStats(3);
			magobliTheme.setLoop(false);
			magobliTheme.stop();
			x = 0;
		}

#pragma region UPDATE_BACKGROUND
		for (int i = 0; i < 10; i++)
		{
			stone[i]->StoneUpdate();
		}
#pragma endregion

#pragma region UPDATE_MAGICIAN_&_ENERGY
		magician->MagicianMove(p);
		magician->MagicianLife(stone, p, gameS);
		magician->MagicianUpdate();

		for (int c = 0; c < 3; c++)
		{
			if (ene[c]->GetEnergyLife() == false && magician->CreateEnergy(p))
			{
				ene[magician->GetCounterEnergy() - 1]->NewEnergy(magician, magician->CreateEnergy(p));
			}
			else
			{
				ene[c]->EnergyLife();
			}
			if (c > 0 && c < 3)
			{
				if (ene[c - 1]->GetEnergyLife() == false && ene[c]->GetEnergyLife() == true)
				{
					ene[c - 1] = ene[c];
				}
				if (ene[c - 1]->GetEnergyLife() == false && ene[c + 1]->GetEnergyLife() == true)
				{
					ene[c - 1] = ene[c + 1];
				}
			}
		}
#pragma endregion

#pragma region UPDATE_GOBLINS_&_BULLET
		for (int d = 0; d < numbG; d++)
		{
			gabs[d]->NewGoblin(gameT->GetTimeGame(), stone);
			gabs[d]->GoblinLife(ene, p, magician);

			bul[d]->NewBullet(gabs[d]);
			bul[d]->BulletLife(p, gameS, magician->GetMagicianPos());
		}
#pragma endregion

		std::cout << "Update game end" << std::endl;

		gameT->EndTime();
#pragma endregion 
		break;
	case 3:
#pragma region UPDADE_GAMEOVER
		if (x == 0)
		{
			magobliTheme.stop();
			menu.setLoop(true);
			menu.play();
			x++;
		}
#pragma endregion 
		break;
	default:
		break;
	}
}

//--------------------------------------------------------------
void ofApp::draw() {
	switch (gameS->GetEstado())
	{
	case 0:
#pragma region DRAW_MENU
		std::cout << "Draw Menu start" << std::endl;

		fundo.draw(0, 0);
		magobli.draw(100, 300, 480, 96);
		start->DrawButton();
		tutorial->DrawButton();
		exit->DrawButton();
		p->DrawAnimationFrame(0);

		std::cout << "Draw Menu end" << std::endl;
#pragma endregion 
		break;
	case 1:
#pragma region DRAW_TUTORIAL
		std::cout << "Draw Tutorial start" << std::endl;

		if (p->GetLevel() < 3)
		{
			for (int i = 0; i < 12; i++)
			{
				if (i < 6)
				{
					for (int j = 0; j < 16; j++)
					{
						terra1.draw((j * 64), (i * 64));
					}
				}
				else if (i == 6)
				{
					for (int j = 0; j < 16; j++)
					{
						terra.draw((j * 64), (i * 64));
					}
				}
				else
				{
					for (int j = 0; j < 16; j++)
					{
						lava.draw((j * 64), (i * 64), 64, 64);
					}
				}
			}
		}
		if (p->GetLevel() == 0)
		{
			tutoKeyUp->DrawAnimation(6 * 64, 6 * 64, 64, 64);
			tutoKeyDown->DrawAnimation(7 * 64, 6 * 64, 64, 64);
			tutoKeyLeft->DrawAnimation(8 * 64, 6 * 64, 64, 64);
			tutoKeyRight->DrawAnimation(9 * 64, 6 * 64, 64, 64);
			tutoKeyZ->DrawAnimation(7 * 64, 4 * 64, 64, 64);
			portal->DrawPortal(8 * 64, 4 * 64);
		}
		else if (p->GetLevel() == 1)
		{
			tutoKeyZ->DrawAnimation(6 * 64, 6 * 64, 64, 64);
			villa[0]->DrawImage();

			portal->DrawPortal(9 * 64, 6 * 64);
		}
		else if (p->GetLevel() == 2)
		{
			tutoKeyX->DrawAnimation(6 * 64, 6 * 64, 64, 64);
			gabs[0]->DrawGoblin(0);
			portal->DrawPortal(960, 0);

			for (int i = 0; i < 3; i++)
			{
				ene[i]->DrawEnergy();
			}
		}
		else if (p->GetLevel() == 3)
		{
			for (int a = 0; a < 12; a++)
			{
				if (a == 0)
				{
					for (int j = 0; j < 16; j++)
					{
						terra.draw((j * 64), (a * 64));
					}
				}
				else if (a == 11)
				{
					for (int j = 0; j < 16; j++)
					{
						terra1.draw((j * 64), (a * 64), 64, 64);
					}
				}
				else
				{
					for (int b = 0; b < 16; b++)
					{
						lava.draw(0 + (b * 64), (a * 64), 64, 64);
					}

					stone[a - 1]->DrawImage();
				}
			}

			portal->DrawPortal(8 * 64, 11 * 64);
		}
		magician->DrawMagician(p);

		std::cout << "Draw Tutorial end" << std::endl;
#pragma endregion
		break;
	case 2:
#pragma region DRAW_GAME
		std::cout << "Draw Game start" << std::endl;

		for (int a = 0; a < 10; a++)
		{
			for (int a = 0; a < 12; a++)
			{
				if (a == 0)
				{
					for (int j = 0; j < 16; j++)
					{
						terra.draw((j * 64), (a * 64));
					}
				}
				else if (a == 11)
				{
					for (int j = 0; j < 16; j++)
					{
						terra1.draw((j * 64), (a * 64), 64, 64);
					}
				}
				else
				{
					for (int b = 0; b < 16; b++)
					{
						lava.draw(0 + (b * 64), (a * 64), 64, 64);
					}

					stone[a - 1]->DrawImage();
				}
			}
		}

		magician->DrawMagician(p);
		portal->DrawPortal(384, 704);

		if (p->GetLevel() == 0)
		{
			for (int a = 0; a < numbV; a++)
			{
				villa[a]->DrawImage(64 * a, 0);
			}
			for (int a = 0; a < numbG; a++)
			{
				gabs[numbG]->DrawGoblin();
			}
		}
		else if (p->GetLevel() == 1)
		{
			for (int a = 0; a < numbV; a++)
			{
				villa[a]->DrawImage(64 * a, 0);
			}
			for (int a = 0; a < numbG; a++)
			{
				gabs[numbG]->DrawGoblin();
			}
		}
		else if (p->GetLevel() == 2)
		{
			for (int a = 0; a < numbV; a++)
			{
				villa[a]->DrawImage(64 * a, 0);
			}
			for (int a = 0; a < numbG; a++)
			{
				gabs[numbG]->DrawGoblin();
			}
		}
		else if (p->GetLevel() == 3)
		{
			for (int a = 0; a < numbV; a++)
			{
				villa[a]->DrawImage(64 * a, 0);
			}
			for (int a = 0; a < numbG; a++)
			{
				gabs[numbG]->DrawGoblin();
			}
		}
		else if (p->GetLevel() == 4)
		{
			for (int a = 0; a < numbV; a++)
			{
				villa[a]->DrawImage(64 * a, 0);
			}
			for (int a = 0; a < numbG; a++)
			{
				gabs[numbG]->DrawGoblin();
			}
		}
		else if (p->GetLevel() == 5)
		{
			for (int a = 0; a < numbV; a++)
			{
				villa[a]->DrawImage(64 * a, 0);
			}
			for (int a = 0; a < numbG; a++)
			{
				gabs[numbG]->DrawGoblin();
			}
		}
		else if (p->GetLevel() == 6)
		{
			for (int a = 0; a < numbV; a++)
			{
				villa[a]->DrawImage(64 * a, 0);
			}
			for (int a = 0; a < numbG; a++)
			{
				gabs[numbG]->DrawGoblin();
			}
		}

		std::cout << "Draw Game end" << std::endl;
#pragma endregion 
		break;
#pragma region
	case 3:
		std::cout << "Draw GameOver start" << std::endl;

		fundo.draw(0, 0);
		gameover.draw(100, 300, 480, 192);

		std::cout << "Draw GameOver end" << std::endl;
		break;
#pragma endregion UPDADE_GAMEOVER
	default:
		break;
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	int z;

	switch (gameS->GetEstado())
	{
#pragma region PRESS_MENU
	case 0:
		if (key == OF_KEY_UP)
			z = 0;
		else if (key == OF_KEY_DOWN)
			z = 1;
		else if (key == 'z')
			z = 4;
		else if (key == 'x')
			z = 5;
		else
			z = 10;
		break;
#pragma endregion 
#pragma region PRESS_GAME
	case 1:
		if (key == OF_KEY_UP)
			z = 0;
		else if (key == OF_KEY_DOWN)
			z = 1;
		else if (key == OF_KEY_RIGHT)
			z = 2;
		else if (key == OF_KEY_LEFT)
			z = 3;
		else if (key == 'z')
			z = 4;
		else if (key == 'x')
			z = 5;
		else
			z = 10;
		break;
	case 2:
		if (key == OF_KEY_UP)
			z = 0;
		else if (key == OF_KEY_DOWN)
			z = 1;
		else if (key == OF_KEY_RIGHT)
			z = 2;
		else if (key == OF_KEY_LEFT)
			z = 3;
		else if (key == 'z')
			z = 4;
		else if (key == 'x')
			z = 5;
		else
			z = 10;
		break;
#pragma endregion 
#pragma region PRESS_GAMEOVER
	case 3:
		if (key == OF_KEY_UP)
			z = 0;
		else if (key == OF_KEY_DOWN)
			z = 1;
		else if (key == 'x')
			z = 5;
		else if (key == 'z')
			z = 4;
		else
			z = 10;
		break;
#pragma endregion 
	default:
		z = 10;
		break;
	}

	p->Press(z);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {
	int z;

	switch (gameS->GetEstado())
	{
#pragma region RELEASE_MENU
	case 0:
		if (key == OF_KEY_UP)
			z = 0;
		else if (key == OF_KEY_DOWN)
			z = 1;
		else if (key == 'x' || key == 'X')
			z = 5;
		else if (key == 'z' || key == 'Z')
			z = 4;
		else
			z = 10;
		break;
#pragma endregion 
#pragma region RELEASE_GAME
	case 1:
		if (key == OF_KEY_UP)
			z = 0;
		else if (key == OF_KEY_DOWN)
			z = 1;
		else if (key == OF_KEY_RIGHT)
			z = 2;
		else if (key == OF_KEY_LEFT)
			z = 3;
		else if (key == 'x')
			z = 5;
		else if (key == 'z')
			z = 4;
		else
			z = 10;

		break;
	case 2:
		if (key == OF_KEY_UP)
			z = 0;
		else if (key == OF_KEY_DOWN)
			z = 1;
		else if (key == OF_KEY_RIGHT)
			z = 2;
		else if (key == OF_KEY_LEFT)
			z = 3;
		else if (key == 'x')
			z = 5;
		else if (key == 'z')
			z = 4;
		else
			z = 10;
		break;
#pragma endregion 
#pragma region RELEASE_GAMEOVER
	case 3:
		if (key == OF_KEY_UP)
			z = 0;
		else if (key == OF_KEY_DOWN)
			z = 1;
		else if (key == GLFW_KEY_ENTER)
			z = 5;
		else if (key == GLFW_KEY_SPACE)
			z = 4;
		else
			z = 10;
		break;
#pragma endregion 
	default:
		z = 10;
		break;
	}

	p->Release(z);
}

#pragma region
//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
#pragma endregion NO
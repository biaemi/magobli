#pragma once

#include "ofMain.h"
#include "Vector2D.h"
#include "GameObject.h"
#include "GameStats.h"
#include "GameTime.h"
#include "Goblin.h"
#include "Magician.h"
#include "Stone.h"
#include "Player.h"
#include "Button.h"
#include "Cursor.h"
#include "Portal.h"
#include "Energy.h"
#include "Bullet.h"
#include "Villagers.h"

class ofApp : public ofBaseApp {

private:
	int k, x;

	int numbG, numbV;

	ofImage magobli;
	ofImage lava;
	ofImage fundo;
	ofImage gameover;
	ofImage terra;
	ofImage terra1;

	GameObject *gameO;
	GameStats *gameS;
	GameTime *gameT;
	Cursor *cursor;
	Button *start;
	Button *tutorial;
	Button *exit;
	Magician *magician;
	Portal *portal;
	Player *p;
	Energy *ene[3];
	Goblin *gabs[4]; // Te amo Gabs <3 Eu juro T--T
	Bullet *bul[4];
	Villagers *villa[4];
	Stone *stone[10];

	GameObject *tutoKeyUp;
	GameObject *tutoKeyDown;
	GameObject *tutoKeyRight;
	GameObject *tutoKeyLeft;
	GameObject *tutoKeyZ;
	GameObject *tutoKeyX;

	ofSoundPlayer menu;
	ofSoundPlayer magobliTheme;

public:
	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
};